package com.sm.godtapp;

import android.app.Application;

import com.sm.godtapp.data.api.ApiComponent;
import com.sm.godtapp.data.api.ApiModule;
import com.sm.godtapp.data.api.DaggerApiComponent;

public class GodtApplication extends Application {

    private ApiComponent apiComponent;

    @Override
    public void onCreate() {
        super.onCreate();

       apiComponent = DaggerApiComponent.builder()
               .apiModule(new ApiModule())
               .build();
    }

    public ApiComponent getApiComponent() {
        return apiComponent;
    }
}
