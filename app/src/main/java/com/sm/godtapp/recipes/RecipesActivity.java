package com.sm.godtapp.recipes;

import android.support.v4.app.Fragment;

import com.sm.godtapp.base.SingleFragmentActivity;

public class RecipesActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new RecipesFragment();
    }

}
