package com.sm.godtapp.recipes;

import com.sm.godtapp.base.BasePresenter;
import com.sm.godtapp.base.BaseView;
import com.sm.godtapp.data.model.RecipeResult;

import java.util.List;

public interface RecipesContract {
    interface View extends BaseView{
        void setRecipes(List<RecipeResult> recipes);
        void showFetchRecipesError();
        void hideProgressBar();
        void showProgressBar();
    }

    interface Presenter extends BasePresenter {

        void getRecipes();
        void filterRecipes(String filter);
    }
}
