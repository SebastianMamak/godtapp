package com.sm.godtapp.recipes;

import android.support.annotation.NonNull;

import com.sm.godtapp.data.api.GodtService;
import com.sm.godtapp.data.model.Recipe;
import com.sm.godtapp.data.model.RecipeResult;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class RecipesPresenter implements RecipesContract.Presenter {

    private GodtService service;
    private RecipesContract.View view;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private List<RecipeResult> recipes;

    RecipesPresenter(GodtService service,  RecipesContract.View view) {
        this.service = service;
        this.view = view;
        recipes = new ArrayList<>();
    }

    @Override
    public void getRecipes() {

        view.showProgressBar();

        if (recipes.size() > 0) {
            view.setRecipes(recipes);
            view.hideProgressBar();
        } else {
            fetchRecipes();
        }

    }

    private void fetchRecipes() {
        compositeDisposable.add(service.getRecipes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<Recipe>>() {
                    @Override
                    public void onSuccess(@NonNull List<Recipe> response) {

                        for (Recipe recipe : response) {

                            RecipeResult recipeResult = new RecipeResult(recipe.getTitle());
                            recipeResult.setImage(recipe.getImages().get(0).getUrl());
                            recipeResult.setDescription(recipe.getDescription());
                            recipeResult.setIngredients(recipe.getIngredientsToDisplay());

                            recipes.add(recipeResult);
                        }

                        view.setRecipes(recipes);
                        view.hideProgressBar();

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        view.hideProgressBar();
                        view.showFetchRecipesError();

                    }
                }));
    }

    @Override
    public void filterRecipes(String filter) {

    }
}
