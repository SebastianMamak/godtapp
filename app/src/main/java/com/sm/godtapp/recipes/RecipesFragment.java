package com.sm.godtapp.recipes;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sm.godtapp.GodtApplication;
import com.sm.godtapp.R;
import com.sm.godtapp.base.BaseFragment;
import com.sm.godtapp.data.model.RecipeResult;
import com.sm.godtapp.data.api.GodtService;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecipesFragment extends BaseFragment implements RecipesContract.View {

    @BindView(R.id.recipes_recycler_view) RecyclerView recipesRecyclerView;
    @BindView(R.id.recipes_progress_bar) ProgressBar progressBar;

    private RecipesAdapter recipesAdapter;

    private RecipesContract.Presenter presenter;

    @Inject
    GodtService service;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((GodtApplication) getActivity()
                .getApplication()).getApiComponent().inject(this);

        setRetainInstance(true);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.recipes_fragment, container, false);

        ButterKnife.bind(this, view);

        recipesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

         if (recipesAdapter == null) {
             recipesAdapter = new RecipesAdapter(new ArrayList<>());
         }

        recipesRecyclerView.setAdapter(recipesAdapter);

        if (presenter == null) {
            presenter = new RecipesPresenter(service, this);
        }

        presenter.getRecipes();

        return view;
    }

    @Override
    public void setRecipes(List<RecipeResult> recipes) {
        recipesAdapter.setRecipes(recipes);
        recipesAdapter.notifyDataSetChanged();
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void showFetchRecipesError() {
        Snackbar.make(getView(), R.string.fetch_recipes_error, Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(R.string.fetch_retry_action), view -> presenter.getRecipes())
                .show();
    }

    class RecipesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_title) TextView titleTextView;
        @BindView(R.id.item_description) TextView descriptionTextView;
        @BindView(R.id.item_ingredients) TextView ingredientsTextView;
        @BindView(R.id.item_image) ImageView photoImageView;

        RecipesViewHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.recipes_list_item, parent, false));
            ButterKnife.bind(this, itemView);
        }

        public void bind(RecipeResult recipe) {
            titleTextView.setText(recipe.getTitle());
            descriptionTextView.setText(recipe.getDescription());
            ingredientsTextView.setText(recipe.getIngredients());

            Picasso.with(getContext())
                    .load(recipe.getImage())
                    .resize(300, 300)
                    .centerCrop()
                    .into(photoImageView);

        }
    }

    private class RecipesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private List<RecipeResult> recipes;

        RecipesAdapter(List<RecipeResult> recipes) {
            this.recipes = recipes;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new RecipesViewHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
            RecipeResult recipe = recipes.get(position);
            RecipesViewHolder recipesViewHolder = (RecipesViewHolder) viewHolder;
            recipesViewHolder.bind(recipe);
        }

        @Override
        public int getItemCount() {
            return recipes.size();
        }

        void setRecipes(List<RecipeResult> recipes) {
            this.recipes = recipes;
        }
    }
}
