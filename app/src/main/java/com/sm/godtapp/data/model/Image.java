package com.sm.godtapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Image {

    @SerializedName("imboId")
    @Expose
    private String imboId;
    @SerializedName("url")
    @Expose
    private String url;

    public String getImboId() {
        return imboId;
    }

    public void setImboId(String imboId) {
        this.imboId = imboId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
