package com.sm.godtapp.data.api;

import com.sm.godtapp.data.model.Recipe;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface GodtService {

    @GET("getRecipesListDetailed?tags=&size=thumbnail-medium&ratio=1&limit=50&from=0")
    public Single<List<Recipe>> getRecipes();
}
