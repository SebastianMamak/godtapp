package com.sm.godtapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Recipe {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("basicPortionNumber")
    @Expose
    private Integer basicPortionNumber;
    @SerializedName("preparationTime")
    @Expose
    private Integer preparationTime;
    @SerializedName("numberOfComments")
    @Expose
    private Integer numberOfComments;
    @SerializedName("numberOfLikes")
    @Expose
    private Integer numberOfLikes;
    @SerializedName("publishedAt")
    @Expose
    private String publishedAt;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("embedTitle")
    @Expose
    private Object embedTitle;
    @SerializedName("canonicalUrl")
    @Expose
    private Object canonicalUrl;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("menyExported")
    @Expose
    private Object menyExported;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("ingredients")
    @Expose
    private List<Ingredient> ingredients = null;
    @SerializedName("tags")
    @Expose
    private List<Tag> tags = null;
    @SerializedName("steps")
    @Expose
    private List<Step> steps = null;
    @SerializedName("images")
    @Expose
    private List<Image> images = null;
    @SerializedName("links")
    @Expose
    private Object links;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getBasicPortionNumber() {
        return basicPortionNumber;
    }

    public void setBasicPortionNumber(Integer basicPortionNumber) {
        this.basicPortionNumber = basicPortionNumber;
    }

    public Integer getPreparationTime() {
        return preparationTime;
    }

    public void setPreparationTime(Integer preparationTime) {
        this.preparationTime = preparationTime;
    }

    public Integer getNumberOfComments() {
        return numberOfComments;
    }

    public void setNumberOfComments(Integer numberOfComments) {
        this.numberOfComments = numberOfComments;
    }

    public Integer getNumberOfLikes() {
        return numberOfLikes;
    }

    public void setNumberOfLikes(Integer numberOfLikes) {
        this.numberOfLikes = numberOfLikes;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getEmbedTitle() {
        return embedTitle;
    }

    public void setEmbedTitle(Object embedTitle) {
        this.embedTitle = embedTitle;
    }

    public Object getCanonicalUrl() {
        return canonicalUrl;
    }

    public void setCanonicalUrl(Object canonicalUrl) {
        this.canonicalUrl = canonicalUrl;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getMenyExported() {
        return menyExported;
    }

    public void setMenyExported(Object menyExported) {
        this.menyExported = menyExported;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Object getLinks() {
        return links;
    }

    public void setLinks(Object links) {
        this.links = links;
    }

    public String getIngredientsToDisplay() {
        StringBuilder builder = new StringBuilder();

        for (Ingredient ingredient : ingredients) {
            builder.append(ingredient.getName()).append(", ");
        }

        return builder.toString();
    }

}