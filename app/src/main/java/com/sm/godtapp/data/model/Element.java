package com.sm.godtapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Element {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("hint")
    @Expose
    private Object hint;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("unitName")
    @Expose
    private String unitName;
    @SerializedName("symbol")
    @Expose
    private String symbol;
    @SerializedName("menyCategory")
    @Expose
    private MenyCategory menyCategory;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Object getHint() {
        return hint;
    }

    public void setHint(Object hint) {
        this.hint = hint;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public MenyCategory getMenyCategory() {
        return menyCategory;
    }

    public void setMenyCategory(MenyCategory menyCategory) {
        this.menyCategory = menyCategory;
    }

}