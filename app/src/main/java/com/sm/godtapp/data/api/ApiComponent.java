package com.sm.godtapp.data.api;

import com.sm.godtapp.recipes.RecipesFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules={ApiModule.class})
public interface ApiComponent {
    void inject(RecipesFragment fragment);
}

