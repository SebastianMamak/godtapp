What have been done
========

* fetch recipes from godt.no API
* display list of the first 50 recipes
* list items contains title, image, description and ingredients names

What left to do
========
* offline mode
* list filter

What could be done better
========
* extract fetchRecipes from presenter to repository
* inject GodtService directly to RecipesPresenter
* inject RecipesPresenter to view
* add unit tests

Dependencies
========
* Retrofit2
* RxJava2
* Dagger2
* ButterKnife
